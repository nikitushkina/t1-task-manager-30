package ru.t1.nikitushkina.tm.exception.user;

public final class ExistLoginException extends AbstractUserException {

    public ExistLoginException() {
        super("Error! Login already exists.");
    }

}
