package ru.t1.nikitushkina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User findByLogin(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    void lockUserByLogin(@Nullable String login);

    @NotNull
    User removeByEmail(@Nullable String email);

    @NotNull
    User removeByLogin(@Nullable String login);

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password);

    void unlockUserByLogin(@Nullable String login);

    @NotNull
    User updateUser(@Nullable String id,
                    @Nullable String firstName,
                    @Nullable String lastName,
                    @Nullable String middleName);

}
