package ru.t1.nikitushkina.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.Domain;
import ru.t1.nikitushkina.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-backup-load";

    @NotNull
    public static final String DESCRIPTION = "Load backup data from xml.";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_BACKUP));
        @NotNull final String xml = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
