package ru.t1.nikitushkina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.model.Project;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-show-by-id";

    @NotNull
    public static final String DESCRIPTION = "Show project by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        @NotNull final String userId = getUserId();
        System.out.println("[ENTER ID]");
        @NotNull final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findOneById(userId, id);
        showProject(project);
    }

}
